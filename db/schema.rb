# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170315144118) do

  create_table "favorite_folders", force: :cascade do |t|
    t.string   "name"
    t.integer  "dispaly_order"
    t.integer  "user_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["user_id"], name: "index_favorite_folders_on_user_id"
  end

  create_table "favorite_movies", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "movie_id"
    t.integer  "favorite_folder_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["movie_id", "user_id"], name: "index_favorite_movies_on_movie_id_and_user_id", unique: true
    t.index ["movie_id"], name: "index_favorite_movies_on_movie_id"
    t.index ["user_id", "favorite_folder_id", "created_at"], name: "index_favorite_movies_on_user_and_folder_and_created_at"
  end

  create_table "linked_sites", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "movies", force: :cascade do |t|
    t.text     "url"
    t.string   "title"
    t.text     "thumbnail"
    t.integer  "play_count",     default: 0
    t.text     "tags"
    t.integer  "user_id"
    t.integer  "linked_site_id"
    t.boolean  "activated",      default: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.index ["created_at", "play_count"], name: "index_movies_on_created_at_and_play_count"
    t.index ["linked_site_id"], name: "index_movies_on_linked_site_id"
    t.index ["tags", "created_at"], name: "index_movies_on_tags_and_created_at"
    t.index ["title", "created_at"], name: "index_movies_on_title_and_created_at"
    t.index ["url", "created_at"], name: "index_movies_on_url_and_created_at"
    t.index ["user_id", "created_at"], name: "index_movies_on_user_id_and_created_at"
    t.index ["user_id"], name: "index_movies_on_user_id"
  end

  create_table "playings", force: :cascade do |t|
    t.integer  "movie_id",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_at", "movie_id"], name: "index_playings_on_created_at_and_movie_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.integer  "importance", default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "user_relationships", force: :cascade do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["followed_id"], name: "index_user_relationships_on_followed_id"
    t.index ["follower_id", "followed_id"], name: "index_user_relationships_on_follower_id_and_followed_id", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.datetime "authenticated_at"
    t.boolean  "admin",            default: false
    t.index ["name"], name: "index_users_on_name", unique: true
  end

end
