User.create!(name: "adminuser", password: "password", password_confirmation: "password", admin: true)

99.times do |n|
  name = "exampleuser#{n+1}"
  User.create!(name: name, password: "password", password_confirmation: "password")
end

LinkedSite.create!(name: "localhost", url: "http://localhost")
LinkedSite.create!(name: "localhost.com", url: "http://localhost.com")

users = User.order(:created_at).take(3)
31.times do |n|
  users.each do |user|
    user.movies.create!(url: "#{LinkedSite.first.url}/#{n}", thumbnail: "#{LinkedSite.first.url}/thumb/#{n}", title: "#{n}番目の投稿動画", tags: ",素人,美少女,", play_count: rand(10000), linked_site_id: LinkedSite.first.id, activated: true)
  end
end
  
users = User.all
user = users.find(2)
following = users[3..50]
followers = users[4..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
