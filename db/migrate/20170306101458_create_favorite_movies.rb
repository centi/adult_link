class CreateFavoriteMovies < ActiveRecord::Migration[5.0]
  def change
    create_table :favorite_movies do |t|
      t.references :user, foreign_key: true
      t.references :movie, foreign_key: true, index: true
      t.references :favorite_folder, foreign_key: true, default: nil

      t.timestamps
    end
    add_index :favorite_movies, [:movie_id, :user_id], unique: true
    add_index :favorite_movies, [:user_id, :favorite_folder_id, :created_at], name: 'index_favorite_movies_on_user_and_folder_and_created_at'
  end
end
