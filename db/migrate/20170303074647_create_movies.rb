class CreateMovies < ActiveRecord::Migration[5.0]
  def change
    create_table :movies do |t|
      t.text :url
      t.string :title
      t.text :thumbnail
      t.integer :play_count, default: 0
      t.text :tags
      t.references :user, foreign_key: true, index: false
      t.references :linked_site, foreign_key: true
      t.boolean :activated, default: false

      t.timestamps
    end
    add_index :movies, [:tags, :created_at]
    add_index :movies, [:title,:created_at]
    add_index :movies, [:url, :created_at]
    add_index :movies, [:user_id, :created_at]
  end
end
