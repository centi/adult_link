class AddAuthenticatedAtToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :authenticated_at, :datetime
  end
end
