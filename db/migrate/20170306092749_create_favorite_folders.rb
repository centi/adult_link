class CreateFavoriteFolders < ActiveRecord::Migration[5.0]
  def change
    create_table :favorite_folders do |t|
      t.string :name
      t.integer :dispaly_order
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :favorite_folders, [:user_id, :display_order, :name]
  end
end
