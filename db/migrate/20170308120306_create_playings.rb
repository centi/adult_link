class CreatePlayings < ActiveRecord::Migration[5.0]
  def change
    create_table :playings do |t|
      t.references :movie, foreign_key: true, null: false

      t.timestamps
    end
    add_index :playings, [:created_at, :movie_id]
  end
end
