class CreateUserRelationships < ActiveRecord::Migration[5.0]
  def change
    create_table :user_relationships do |t|
      t.integer :follower_id, index: false
      t.integer :followed_id, index: true

      t.timestamps
    end
    add_index :user_relationships, [:follower_id, :followed_id], unique: true
  end
end
