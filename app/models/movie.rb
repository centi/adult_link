class Movie < ApplicationRecord
  belongs_to :user
  belongs_to :linked_site
  has_many :favorite_movie, dependent: :destroy
  has_many :playing, dependent: :destroy
  
  default_scope -> { order(created_at: :desc) }
  
  validates :user_id, presence: true
  validate :validate_title
  validate :validate_url
  validate :validate_thumbnail
  validate :validate_tags
  
  def self.search(word)
    Movie.where(["tags like ? or title like ?", "%#{word}%", "%#{word}%"]) if word.present?
  end
  
  private
  
    def validate_title
      if title.blank?
        errors.add(:title, "入力する必要があります")
      elsif title.length > 40
        errors.add(:title, "４０文字以内で入力してください")
      end
    end
  
    def validate_url
      if url.blank?
        errors.add(:url, "入力する必要があります")
      end
    end

    def validate_thumbnail
      if thumbnail.blank?
        errors.add(:thumbnail, "入力する必要があります")
      end
    end

    def validate_tags
      if !tags.match(/\A,([^\s　,]+,)+\z/)
        errors.add(:tags, "タグの形式が正しくありません")
      end
    end
end
