class FavoriteMovie < ApplicationRecord
  belongs_to :user
  belongs_to :movie
  belongs_to :favorite_folder, required: false
  
  validates :user_id, uniqueness: { scope: :movie_id }
end
