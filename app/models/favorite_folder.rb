class FavoriteFolder < ApplicationRecord
  belongs_to :user
  has_many :favorite_movie
  
  validate :validate_name
  
  private
  
    def validate_name
      if name.blank?
        errors.add(:name, "入力されていません")
      elsif name.length > 20
        errors.add(:name, "２０文字以内で入力してください")
      end
    end
end
