class User < ApplicationRecord
  attr_accessor :remember_token
  
  has_many :movies
  has_many :active_relationships, class_name: "UserRelationship", foreign_key: "follower_id", dependent: :destroy
  has_many :passive_relationships, class_name: "UserRelationship", foreign_key: "followed_id", dependent: :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships
  has_many :favorite_movies, dependent: :destroy
  has_many :favorites, through: :favorite_movies, source: :movie
  has_many :favorite_folders, dependent: :destroy
  
  # ハッシュ化パスワード認証機能を使用するための設定関数を実行
  has_secure_password
  
  validate :validate_name, on: :create
  validate :validate_password
  
  # 渡された文字列のハッシュ値を返す
  def self.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  def self.new_token
    SecureRandom.urlsafe_base64
  end
  
  # ログイン情報記憶機能のためにDBにトークンを記録する
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
    update_attribute(:authenticated_at, Time.now)
  end
  
  # 渡されたトークンがダイジェストと一致したらtrueを返す
  def authenticated?(token)
    BCrypt::Password.new(remember_digest).is_password?(token) if remember_digest
  end
  
  # ログイン情報記憶機能のためのトークンを破棄する
  def forget
    update_attribute(:remember_digest, nil)
  end
  
  def follow(other_user)
    active_relationships.create(followed_id: other_user.id)
  end
  
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end
  
  # 引数で渡されたuserをフォローしていればtrueを返す
  def following?(other_user)
    following.include?(other_user)
  end
  
  # 動画をお気に入りに追加する
  def add_to_favorites(movie)
    favorite_movies.create(movie_id: movie.id)
  end
  
  # 動画をお気に入りから削除する
  def remove_from_favorites(movie)
    favorite_movie = favorite_movies.find_by(movie_id: movie.id)
    return false unless favorite_movie
    favorite_movie.destroy
  end
  
  private
  
    def validate_name
      if name.blank?
        errors.add(:name, "入力する必要があります")
      elsif !((6..15) === name.length)
        errors.add(:name, "６から１５文字で入力してください")
      elsif !name.match(/\A[0-9a-zA-Z_]+\z/)
        errors.add(:name, "半角英数字,アンダースコアのみで入力してください")
      elsif User.exists?(name: name)
        errors.add(:name, "#{name}はすでに登録されています")
      end
    end
  
    def validate_password
      if password.blank?
        errors.add(:password, "入力する必要があります")
      elsif !((6..20) === password.length)
        errors.add(:password, "６から２０文字で入力してください")
      end
    end
end
