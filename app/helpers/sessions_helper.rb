module SessionsHelper
  
  def log_in(user)
    session[:user_id] = user.id
  end
  
  # cookieでログインを試み、成功の場合userを返す
  def log_in_automatically
    if user_id = cookies.signed[:user_id]
      user = User.find_by(id: user_id)
      if user && user.authenticated?(cookies[:remember_token]) && user.authenticated_at >= (Time.now - remaining_seconds)
        log_in user
        @current_user = user
      end
    end
  end
  
  # userのログイン情報を記憶する
  def remember(user)
    user.remember
    cookies.signed[:user_id] = { value: user.id, expires: Time.now + remaining_seconds, path: '/login' }
    cookies[:remember_token] = { value: user.remember_token, expires: Time.now + remaining_seconds, path: '/login' }
  end
  
  # 記録したログイン情報を削除
  def forget
    current_user.forget
    cookies[:user_id] = { value: '', expires: Time.at(0), path: '/login' }
    cookies[:remember_token] = { value: '', expires: Time.at(0), path: '/login' }
  end
  
  # 渡されたユーザーが、ログインしているアカウントと同じならtrueを返す
  def current_user?(user)
    user == current_user
  end
  
  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end
  
  def logged_in?
    !current_user.nil?
  end
  
  def log_out
    forget
    session.delete(:user_id)
    @current_user = nil
  end
  
  # ログイン情報を記憶する秒数を返す
  def remaining_seconds
    60 * 60 * 24 * 30
  end
  
  # 記憶しておいたURL（もしくは引数の値）にリダイレクトする
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end
  
  # アクセスしようとしたURLを記憶しておく
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end
