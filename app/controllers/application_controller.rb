class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  
  private
  
    # ログイン済みでなければログインページにリダイレクトする
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "ログインが必要な操作です"
        redirect_to login_url
      end
    end
  
    # 管理者でなければホームにリダイレクトする
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end
