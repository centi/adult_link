class StaticPagesController < ApplicationController
  def home
    @movies = Movie.limit(10)
    @popular_movies = Movie.joins(:playing).where("playings.created_at": (1.day.ago)..(Time.now)).group("playings.movie_id").reorder("count(playings.movie_id) desc").limit(10)
  end
end
