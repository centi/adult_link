class SessionsController < ApplicationController
  def new
    redirect_to current_user if log_in_automatically
  end
  
  def create
    user = User.find_by(name: params[:session][:name])
    if user && user.authenticate(params[:session][:password])
      log_in user
      params[:session][:remember_me] == '1' ? remember(user) : forget
      redirect_back_or user
    else
      flash.now[:danger] = 'ユーザー名とパスワードの組み合わせが正しくありません'
      render 'new'
    end
  end
  
  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
