class FavoriteMoviesController < ApplicationController
  
  before_action :logged_in_user
  before_action :correct_user, only: [:destroy]
  
  def create
    @movie = Movie.find(params[:movie_id])
    current_user.add_to_favorites(@movie)
    
    respond_to do |format|
      format.html { redirect_to request.referrer || root_url }
      format.js
    end
  end
  
  def destroy
    @movie = Movie.find(params[:movie_id])
    current_user.remove_from_favorites(@movie)
    
    respond_to do |format|
      format.html { redirect_to request.referrer || root_url }
      format.js
    end
  end
  
  def update_for
    folder = FavoriteFolder.find(params[:folder_id])
    params[:favorite_movie_ids].each do |id|
      move_to_folder(folder, FavoriteMovie.find(id))
    end
    redirect_to request.referer || favorites_user_url(current_user)
  end
  
  private
    
    # お気に入り動画をフォルダーに移動。動画かフォルダーがカレントユーザーのものでなければfalseを返す
    def move_to_folder(folder, favorite_movie)
      return unless current_user?(folder.user) && current_user?(favorite_movie.user)
      favorite_movie.update(favorite_folder_id: folder.id)
    end
  
    def correct_user
      @favorite_movie = current_user.favorite_movies.find_by(movie_id: params[:movie_id])
      redirect_to root_url unless @favorite_movie
    end
end
