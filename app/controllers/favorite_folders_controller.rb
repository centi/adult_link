class FavoriteFoldersController < ApplicationController
  
  before_action :logged_in_user
  before_action :correct_user, except: [:new, :create]
  
  def new
    @folder = FavoriteFolder.new
  end
  
  def create
    @folder = FavoriteFolder.new(folder_params.merge(user_id: current_user.id))
    if @folder.save
      flash[:success] = "#{@folder.name}フォルダーを追加しました。"
      redirect_to favorites_user_url(current_user)
    else
      render 'new'
    end
  end
  
  def edit
  end
  
  def update
    if @folder.update_attributes(folder_params)
      flash[:success] = "フォルダー名を変更しました。"
      redirect_to favorites_user_url(current_user)
    else
      render 'edit'
    end
  end
  
  def destroy
    @folder.destroy
    flash[:success] = "フォルダーを削除しました"
    redirect_to favorites_user_url(current_user)
  end
  
  private
  
    def folder_params
      params.require(:favorite_folder).permit(:name)
    end
  
    def correct_user
      @folder = FavoriteFolder.find(params[:id])
      redirect_to root_url unless current_user?(@folder.user)
    end
end
