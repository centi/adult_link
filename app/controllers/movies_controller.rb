class MoviesController < ApplicationController
  
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :admin_user, only: [:destroy]
  
  def index
    @movies = Movie.paginate(page: params[:page])
  end
  
  def show
    @movie = Movie.find(params[:id])
    unless request.referrer == request.original_url
      @movie.increment!(:play_count)
      Playing.create(movie_id: @movie.id)
    end
  end
  
  def create
    @movie = Movie.new(movie_params)
    @movie.save
  end
  
  def destroy
    Movie.find_by(id: params[:id]).destroy
    flash[:success] = "動画を削除しました。"
    redirect_to request.referrer || root_url
  end
  
  def rank
    @movies = Movie.reorder('play_count desc').paginate(page: params[:page])
    render :index
  end
  
  def day
    @movies = Movie.joins(:playing).where("playings.created_at": (1.day.ago)..(Time.now)).group("playings.movie_id").reorder("count(playings.movie_id) desc").paginate(page: params[:page])
    render :index
  end
  
  def week
    @movies = Movie.joins(:playing).where("playings.created_at": (7.days.ago)..(Time.now)).group("playings.movie_id").reorder("count(playings.movie_id) desc").paginate(page: params[:page])
    render :index
  end
  
  def month
    @movies = Movie.joins(:playing).where("playings.created_at": (30.days.ago)..(Time.now)).group("playings.movie_id").reorder("count(playings.movie_id) desc").paginate(page: params[:page])
    render :index
  end
  
  def search
    @movies = Movie.search(params[:search])
    if @movies.present?
      @movies = @movies.paginate(page: params[:page])
      flash.now[:success] = "”#{params[:search]}”の検索結果。"
      render :index
    else
      flash[:danger] = "”#{params[:search]}”を含む動画は見つかりませんでした。"
      redirect_to movies_path
    end
  end
  
  private
  
    def movie_params
      params.require(:movie).permit(:url, :title, :description, :thumbnail, :play_count, :tags, :user_id, :linked_site_id)
    end
end
