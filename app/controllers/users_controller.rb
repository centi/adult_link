class UsersController < ApplicationController
  
  before_action :logged_in_user,  only: [:index, :edit, :update, :destroy, :following, :followers, :favorites, :following_movies, :folder_edit]
  before_action :correct_user,    only: [:edit, :update, :following, :followers, :favorites, :following_movies, :folder_edit]
  before_action :admin_user,      only: [:index, :destroy]
  
  def index
    @users = User.paginate(page: params[:page])
  end
  
  def show
    @user = User.find(params[:id])
    @movies = @user.movies.paginate(page: params[:page])
    # todo 統合テストを生成して書く
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(params.require(:user).permit(:name, :password, :password_confirmation))
    if @user.save
      log_in @user
      flash[:success] = "はじめまして#{@user.name}さん！"
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def edit
  end
  
  def update
    if @user.authenticate(params[:user][:old_password])
      if @user.update_attributes(user_params)
        flash[:success] = "パスワードを変更しました"
        redirect_to @user
      else
        render 'edit'
      end
    else
      @user.errors.add(:old_password, "今までのパスワードが正しくありません")
      render 'edit'
    end
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "ユーザーを削除しました"
    redirect_to users_url
  end
  
  def following
    @title = "フォロー中のユーザー"
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
  end
  
  def followers
    @title = "フォロワー"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render :following
  end
  
  # フォローしているユーザーのアップした動画
  def following_movies
    @user = User.find(params[:id])
    following_ids = "select followed_id from user_relationships where follower_id = #{@user.id}"
    @movies = Movie.where("user_id IN (#{following_ids})").paginate(page: params[:page])
  end
  
  # お気に入り動画リスト
  def favorites
    @movies = Movie.where(id: current_favorite_movie.select('movie_id')).paginate(page: params[:pate])
    @folders = @user.favorite_folders.all
    @movie = nil
  end
  
  # お気に入り動画をフォルダーに移動するフォームビューを描画
  def folder_edit
    @favorite_movies = current_favorite_movie.paginate(page: params[:page])
    @favorite_folder_array = unselected_favorite_folder
  end
  
  private
  
    def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end
  
    # リクエストされたページの正しいユーザーでなければホームにリダイレクトする
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
  
    # folder_idパラメータで指定されたフォルダ内のお気に入り動画を返す
    def current_favorite_movie
      @folder_id = params[:folder_id]
      FavoriteMovie.where(user_id: current_user, favorite_folder_id: @folder_id)
    end
  
    # ビューのselect下のoption用、カレントフォルダー以外のフォルダーの配列を返す
    def unselected_favorite_folder
      folders = []
      folders << ["フォルダーに分類しない", nil] if params[:folder_id]
      current_user.favorite_folders.where.not('favorite_folders.id': params[:folder_id]).each do |folder|
        folders << [folder.name, folder.id]
      end
      
      folders
    end
end
