require 'test_helper'

class SessionsHelperTest < ActionView::TestCase

  def setup
    @user = users(:testuser1)
    remember(@user)
  end

  test "current_user returns right user when session is nil" do
    log_in_automatically
    assert_equal @user, current_user
    assert is_logged_in?
  end

  test "current_user returns nil when remember digest is wrong" do
    @user.update_attribute(:remember_digest, User.digest(User.new_token))
    log_in_automatically
    assert_nil current_user
  end
  
  test "log_in_automatically returns user when it is within the period" do
    @user.update_attribute(:authenticated_at, Time.now - remaining_seconds + 3)
    assert_equal @user, log_in_automatically
  end
  
  test "log_in_automatically returns nil when it is outside the period" do
    @user.update_attribute(:authenticated_at, Time.now - remaining_seconds - 1)
    assert_nil log_in_automatically
  end
end