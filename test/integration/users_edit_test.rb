require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:testuser1)
  end
  
  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { password: "foo", password_confirmation: "bar", old_password: "password" }}
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { password: "foobar", password_confirmation: "foobar", old_password: "invalid" }}
    assert_template 'users/edit'
  end
  
  test "successful edit with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    password = "foobar"
    patch user_path(@user), params: { user: { password: password, password_confirmation: password, old_password: "password" }}
    assert flash.any?
    assert_redirected_to @user
    @user.reload
    assert @user.authenticate(password)
  end
end
