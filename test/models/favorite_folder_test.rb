require 'test_helper'

class FavoriteFolderTest < ActiveSupport::TestCase
  
  def setup
    @folder = FavoriteFolder.new(user: users(:testuser1), name: "分類します！")
  end
  
  test "should be valid" do
    assert @folder.valid?
  end
  
  test "name should be present" do
    @folder.name = "  　"
    assert_not @folder.valid?
  end
  
  test "name should not be too long" do
    @folder.name = "a" * 21
    assert_not @folder.valid?
  end
end
