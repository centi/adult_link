require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.new(name: "ExampleUser", password: "foobar", password_confirmation: "foobar")
  end
  
  test "should be valid" do
    assert @user.valid?
  end
  
  test "name should be present" do
    @user.name = "   "
    assert_not @user.valid?
  end
  
  test "name should not be too long" do
    @user.name = "a" * 16
    assert_not @user.valid?
  end
  
  test "name should not be too short" do
    @user.name = "a" * 5
    assert_not @user.valid?
  end
  
  test "name should not contain invalid characters" do
    @user.name = "a ftildf"
    assert_not @user.valid?
    @user.name = "a-invalid"
    assert_not @user.valid?
    @user.name = "これだめなやつ"
    assert_not @user.valid?
  end
  
  test "name should be unique" do
    duplicate_user = @user.dup
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "password should be present" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end
  
  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?('')
  end
  
  test "should follow and unfollow a user" do
    user1 = users(:testuser1)
    user2 = users(:testuser2)
    assert_not user1.following?(user2)
    user1.follow(user2)
    assert user1.following?(user2)
    assert user2.followers.include?(user1)
    user1.unfollow(user2)
    assert_not user1.following?(user2)
  end
  
  test "should add and remove a favorite movie" do
    user = users(:adminuser)
    movie = movies(:one)
    assert_not user.favorites.include?(movie)
    user.add_to_favorites(movie)
    assert user.favorites.include?(movie)
    user.remove_from_favorites(movie)
    assert_not user.favorites.include?(movie)
  end
end
