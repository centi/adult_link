require 'test_helper'

class UserRelationshipTest < ActiveSupport::TestCase
  
  def setup
    @relationship = UserRelationship.new(follower_id: users(:testuser1).id, followed_id: users(:testuser2).id)
  end
  
  test "should be vailid" do
    assert @relationship.valid?
  end
  
  test "should require a follower_id" do
    @relationship.follower_id = nil
    assert_not @relationship.valid?
  end
  
  test "should require a followed_id" do
    @relationship.followed_id = nil
    assert_not @relationship.valid?
  end
end
