require 'test_helper'

class FavoriteMovieTest < ActiveSupport::TestCase
  
  def setup
    @favorite_movie = FavoriteMovie.new(user_id: users(:testuser1).id, movie_id: movies(:two).id, favorite_folder_id: favorite_folders(:one).id)
  end
  
  test "should be valid" do
    assert @favorite_movie.valid?
  end
  
  test "should require a user_id" do
    @favorite_movie.user_id = nil
    assert_not @favorite_movie.valid?
  end
  
  test "should require a movie_id" do
    @favorite_movie.movie_id = nil
    assert_not @favorite_movie.valid?
  end
  
  test "should add success without a favorite_folder_id" do
    @favorite_movie.favorite_folder_id = nil
    assert @favorite_movie.valid?
  end
  
  test "user and movie should be unique" do
    duplicate_favorite = @favorite_movie.dup
    @favorite_movie.save
    assert_not duplicate_favorite.valid?
  end
end
