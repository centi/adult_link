require 'test_helper'

class MovieTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:adminuser)
    @site = linked_sites(:localhost)
    @movie = @user.movies.build(url: "http://localhost", title: "あんなエロ動画", thumbnail: "http://localhost", play_count: 0, tags: ",素人,美少女,", linked_site_id: @site.id)
  end
  
  test "should be valid" do
    assert @movie.valid?
  end
  
  test "user id should be present" do
    @movie.user_id = nil
    assert_not @movie.valid?
  end
  
  test "site id should be present" do
    @movie.linked_site_id = nil
    assert_not @movie.valid?
  end
  
  test "title should be present" do
    @movie.title = "  "
    assert_not @movie.valid?
  end
  
  test "title should not too long" do
    @movie.title = "あ" * 41
    assert_not @movie.valid?
  end
  
  test "url should be present" do
    @movie.url = "  "
    assert_not @movie.valid?
  end
  
  test "thumbnail should be present" do
    @movie.thumbnail = "  "
    assert_not @movie.valid?
  end
  
  test "tags should be right format" do
    @movie.tags = ", 　,"
    assert_not @movie.valid?
  end
  
  test "order should be most recent first" do
    assert_equal movies(:most_recent), Movie.first
  end
end
