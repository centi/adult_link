require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @admin = users(:adminuser)
    @user = users(:testuser1)
    @other_user = users(:testuser2)
  end
  
  test "should get new" do
    get signup_path
    assert_response :success
  end
  
  test "should redirect index when not logged in" do
    get users_path
    assert_redirected_to login_url
  end
  
  test "should redirect index when logged in as a non-admin" do
    log_in_as(@user)
    get users_path
    assert_redirected_to root_url
  end
  
  test "should redirect edit when not logged in" do
    get edit_user_path(@user)
    assert flash.any?
    assert_redirected_to login_url
  end
  
  test "should redirect update when not logged in" do
    patch user_path(@user), params: { user: { password: "foobar" }}
    assert flash.any?
    assert_redirected_to login_url
  end
  
  test "should redirect edit when logged in as wrong user" do
    log_in_as(@other_user)
    get edit_user_path(@user)
    assert flash.empty?
    assert_redirected_to root_url
  end
  
  test "should redirect update when logged in as wrong user" do
    log_in_as(@other_user)
    patch user_path(@user), params: { user: { password: "foobar" }}
    assert flash.empty?
    assert_redirected_to root_url
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete user_path(@user)
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@other_user)
    assert_no_difference 'User.count' do
      delete user_path(@user)
    end
    assert_redirected_to root_url
  end 
  
  test "should redirect following when not logged in" do
    get following_user_url(@user)
    assert_redirected_to login_url
  end
  
  test "should redirect following when logged in as other user" do
    log_in_as(@other_user)
    get following_user_url(@user)
    assert_redirected_to root_url
  end
  
  test "should redirect followers when not logged in" do
    get followers_user_url(@user)
    assert_redirected_to login_url
  end
  
  test "should redirect followers when logged in as other user" do
    log_in_as(@other_user)
    get followers_user_url(@user)
    assert_redirected_to root_url
  end
  
  test "should redirect favorites when not logged in" do
    get favorites_user_url(@user)
    assert_redirected_to login_url
  end
  
  test "should redirect fovorites when logged in as other user" do
    log_in_as(@other_user)
    get favorites_user_url(@user)
    assert_redirected_to root_url
  end
  
  test "should redirect following_movies when not logged in" do
    get following_movies_user_url(@user)
    assert_redirected_to login_url
  end
  
  test "should redirect following_movies when logged in as other user" do
    log_in_as(@other_user)
    get following_movies_user_url(@user)
    assert_redirected_to root_url
  end
end
