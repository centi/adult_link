require 'test_helper'

class MoviesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @movie = movies(:one)
    @site = linked_sites(:localhost)
    @user = users(:testuser1)
    @params = { movie: { url: "http://localhost", title: "あんなエロ動画", thumbnail: "http://localhost", tags: ",素人,美少女,", linked_site_id: @site.id, user_id: @user.id }}
  end
  
  test "should be successful create when logged in" do
    log_in_as(@user)
    assert_difference "Movie.count", 1 do
      post movies_path, params: @params
    end
  end
  
  test "should redirect create when not logged in" do
    assert_no_difference 'Movie.count' do
      post movies_path, params: @params
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'Movie.count' do
      delete movie_path(@movie)
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy when logged in as non-admin" do
    log_in_as(@user)
    assert_no_difference 'Movie.count' do
      delete movie_path(@movie)
    end
    assert_redirected_to root_url
  end
  
  test "should be successful search" do
    get search_path(params: { search: "美少女" })
    assert_response :success
    assert flash[:success].present?
  end
  
  test "should redirect search when not found" do
    get search_path(params: { search: "a f" })
    assert_redirected_to movies_path
    assert flash[:danger].present?
  end
  
  test "should redirect search when given an empty string" do
    get search_path(params: { search: "" })
    assert_redirected_to movies_path
    assert flash[:danger].present?
  end
end
