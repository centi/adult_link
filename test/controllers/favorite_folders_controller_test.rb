require 'test_helper'

class FavoriteFoldersControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:testuser1)
    @other_user = users(:testuser2)
    @folder = favorite_folders(:one)
    @params = { favorite_folder: { name: "testfolder" } }
  end
  
  test "should redirect new when not logged in" do
    get new_favorite_folder_url()
    assert_redirected_to login_url
  end
  
  test "should be successful create when logged in user" do
    log_in_as(@user)
    assert_difference "FavoriteFolder.count", 1 do
      post favorite_folders_url(@params)
    end
  end
  
  test "should redirect create when not logged in" do
    post favorite_folders_url()
    assert_redirected_to login_url
  end
  
  test "should redirect edit when not logged in" do
    get edit_favorite_folder_url(@folder)
    assert_redirected_to login_url
  end
  
  test "should redirect edit when logged in as other user" do
    log_in_as(@other_user)
    get edit_favorite_folder_url(@folder)
    assert_redirected_to root_url
  end
  
  test "should be successful update when logged in as correct user" do
    log_in_as(@user)
    patch favorite_folder_url(@folder, params: @params)
    @folder.reload
    assert_equal @folder.name, @params[:favorite_folder][:name]
  end
  
  test "should redirect update when not logged in" do
    patch favorite_folder_url(@folder)
    assert_redirected_to login_url
  end
  
  test "should redirect update when logged in as other user" do
    log_in_as(@other_user)
    patch favorite_folder_url(@folder)
    assert_redirected_to root_url
  end
  
  test "should be successful destroy when logged in as correct user" do
    log_in_as(@user)
    assert_difference "FavoriteFolder.count", -1 do
      delete favorite_folder_url(@folder)
    end
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'FavoriteFolder.count' do
      delete favorite_folder_url(@folder)
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy when logged in as other user" do
    log_in_as(@other_user)
    assert_no_difference 'FavoriteFolder.count' do
      delete favorite_folder_url(@folder)
    end
    assert_redirected_to root_url
  end
end
