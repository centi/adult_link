require 'test_helper'

class FavoriteMoviesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:testuser1)
    @other_user = users(:testuser2)
    @favorite_movie = favorite_movies(:one)
    @params = { movie_id: movies(:one) }
  end
  
  test "create should succeed" do
    log_in_as(@other_user)
    assert_difference 'FavoriteMovie.count', 1 do
      post favorite_movies_path(params: @params)
    end
  end
  
  test "create should require logged in user" do
    assert_no_difference 'FavoriteMovie.count' do
      post favorite_movies_path
    end
    assert_redirected_to login_url
  end
  
  test "update_for should succeed" do
    log_in_as(@other_user)
    patch update_for_favorite_movies_path(params: {folder_id: favorite_folders(:two), favorite_movie_ids: [favorite_movies(:two)]})
    assert_not favorite_movies(:two).reload.favorite_folder.nil?
  end
  
  test "update_for should require logged in user" do
    patch update_for_favorite_movies_path
    assert_redirected_to login_url
  end
  
  test "update_for should require right folder" do
    log_in_as(@other_user)
    patch update_for_favorite_movies_path(params: {folder_id: favorite_folders(:one), favorite_movie_ids: [favorite_movies(:two)]})
    assert favorite_movies(:two).reload.favorite_folder.nil?
  end
  
  test "update_for should require right favorite movie" do
    log_in_as(@other_user)
    patch update_for_favorite_movies_path(params: {folder_id: favorite_folders(:two), favorite_movie_ids: [favorite_movies(:one)]})
    assert favorite_movies(:two).reload.favorite_folder.nil?
  end
  
  test "destroy should succeed" do
    log_in_as(@user)
    assert_difference 'FavoriteMovie.count', -1 do
      delete favorite_movie_path(@favorite_movie, params: @params)
    end
  end
  
  test "destroy should require logged in user" do
    assert_no_difference 'FavoriteMovie.count' do
      delete favorite_movie_path(@favorite_movie, params: @params)
    end
    assert_redirected_to login_url
  end
  
  test "destroy should require logged in as right user" do
    log_in_as(@other_user)
    assert_no_difference 'FavoriteMovie.count' do
      delete favorite_movie_path(@favorite_movie, params: @params)
    end
    assert_redirected_to root_url
  end
end
