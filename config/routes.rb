Rails.application.routes.draw do

  root    'static_pages#home'
  get     '/signup',  to: 'users#new'
  post    '/signup',  to: 'users#create'
  get     '/login',   to: 'sessions#new'
  post    '/login',   to: 'sessions#create'
  delete  '/logout',  to: 'sessions#destroy' 
  get     '/search',  to: 'movies#search',  as: :search
  resources :user_relationships,  only: [:create, :destroy]
  resources :favorite_folders,    except: [:index, :show]
  resources :favorite_movies,     only: [:create, :destroy] do
    patch :update_for, on: :collection
  end
  resources :movies do
    collection do
      get 'month', 'week', 'day', 'rank'
    end
  end
  resources :users do
    member do
      get :following, :followers, :favorites, :following_movies, :folder_edit
    end
  end
end
